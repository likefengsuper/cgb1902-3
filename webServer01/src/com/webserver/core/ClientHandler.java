package com.webserver.core;

import com.webserver.http.HttpRequest;
import com.webserver.http.HttpResponse;

import java.awt.print.Pageable;
import java.io.*;
import java.net.Socket;

/**
 * Socket 处理类,由他进行处理，将服务进行分发~
 */
public class ClientHandler implements Runnable {
    //1. 创建 一个socket对象
    private Socket socket;

    public ClientHandler(Socket socket) {
        this.socket = socket;
    }

    //2.通过线程来处理
    @Override
    public void run() {
        try {
            HttpRequest httpRequest = new HttpRequest(socket);
            HttpResponse httpResponse = new HttpResponse(socket);
            //1.获得解析的url
            String url = httpRequest.getUrl();
            File file = new File("webapps"+url);
            if(file.exists()){
                httpResponse.setEntity( file );
                System.out.println("响应完毕");
            }else {
                System.out.println("该资源不存在 404");
                File notFoundpage = new File("webapps/root/404.html");
                    httpResponse.setStatusCode(404);
                    httpResponse.setEntity(notFoundpage);
            }
            httpResponse.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


}
