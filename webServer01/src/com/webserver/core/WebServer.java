package com.webserver.core;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * 服务类,开启线程将服务进行分发
 */
public class WebServer {
    //1.创建服务对象
    ServerSocket server;

    public WebServer() {
        try {
            server = new ServerSocket(8088);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    //服务器开始工作
        public void start(){
        try{
            while (true) {
            //2.采作阻塞IO方法.拿到socket
            System.out.println("服务器开始工作....");
           Socket socket = server.accept();
            System.out.println("连接成功>>>>>>");
        //将请求传给ClientHandler由他去通过线程处理
            //3.交给Handler 处理
            ClientHandler client = new ClientHandler(socket);
            Thread t = new Thread(client);
            t.start();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        }

//
public static void main(String[] args){
    WebServer webServer = new WebServer();
    webServer.start();
}
}
