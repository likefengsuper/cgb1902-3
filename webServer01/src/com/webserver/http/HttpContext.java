package com.webserver.http;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpContext {
    /**
     * 状态代码与描述的对应关系
     * key:状态代码
     * value:对应的状态描述
     */
    private static final Map<Integer, String> STATUS_MAPPING = new HashMap<>();


    /**
     * 资源后缀与Content-Type值的对应关系
     * key:后缀名，如:png
     * value:Content-Type对应值，如:image/png
     */
    private static Map<String, String> MIME_MAPPING = new HashMap<>();

    static {
        initStatusMapping();
        initMimeMapping();
    }

    //初始化了状态代码与对应描述
    private static void initStatusMapping() {
        STATUS_MAPPING.put(200, "OK");
        STATUS_MAPPING.put(201, "Created");
        STATUS_MAPPING.put(202, "Accepted");
        STATUS_MAPPING.put(204, "No Content");
        STATUS_MAPPING.put(301, "Moved Permanently");
        STATUS_MAPPING.put(302, "Moved Temporarily");
        STATUS_MAPPING.put(304, "Not Modified");
        STATUS_MAPPING.put(400, "Bad Request");
        STATUS_MAPPING.put(401, "Unauthorized");
        STATUS_MAPPING.put(403, "Forbidden");
        STATUS_MAPPING.put(404, "Not Found");
        STATUS_MAPPING.put(500, "Internal Server Error");
        STATUS_MAPPING.put(501, "Not Implemented");
        STATUS_MAPPING.put(502, "Bad Gateway");
        STATUS_MAPPING.put(503, "Service Unavailable");
    }

    //初始化了资源后缀与对应Content-Type的值
    private static void initMimeMapping() {
        /*MIME_MAPPING.put("png", "image/png");
        MIME_MAPPING.put("gif", "image/gif");
        MIME_MAPPING.put("jpg", "image/jpeg");
        MIME_MAPPING.put("css", "text/css");
        MIME_MAPPING.put("html", "text/html");
        MIME_MAPPING.put("js", "application/javascript");*/

        try{
            //用sax解析 SAX
           SAXReader reader = new SAXReader();
           //获取dom对像
            Document doc = reader.read(new File("conf/web.xml"));
             Element root = doc.getRootElement();
            List<Element> mimeList = root.elements("mime-mapping");
            for(Element mimeEle :mimeList ){
                String key = mimeEle.elementText("extension");
                String value = mimeEle.elementText("mime-type");
                MIME_MAPPING.put(key, value);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("size:"+MIME_MAPPING.size());
    }

    /**
     * 根据状态代码获取对应的状态描述默认值
     *
     * @param statusCode
     * @return
     */
    public static String getStatusReason(int statusCode) {
        return STATUS_MAPPING.get(statusCode);
    }

    /**
     * 根据资源后缀名获取对应的Content-Type的值
     *
     * @param ext
     * @return
     */
    public static String getMineType(String ext) {
        return MIME_MAPPING.get(ext);
    }
}
