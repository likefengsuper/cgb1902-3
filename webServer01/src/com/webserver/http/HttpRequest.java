package com.webserver.http;

import java.io.InputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * 处理请求,解析行 解析头,解析body
 */
public class HttpRequest {
    //请求方式
    private String method;
    //请求路径
    private String url;
    //请求协议版本
    private String protocol;

    /*
     * url中的请求部分
     */
    private String requestURI;
    /*
     * url中的参数部分
     */
    private String queryString;
    private Map<String, String> headers = new HashMap<>();

    private Map<String, String> parameters = new HashMap<>();
    private Socket socket;
    private InputStream in;

    public HttpRequest(Socket socket) {
        try {
            this.socket = socket;
            this.in = socket.getInputStream();

            // 解析行
            parseRequestLine();
            //解析头
            parseHanders();
            //解析body
            parseContent();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseContent() {
        System.out.println("解析消息正文....");
        System.out.println("解析完毕......");
    }

    private void parseHanders() {
        System.out.println("解析消息头.....");
        while (true) {
            String line = readLine();
            if ("".equals(line)) {
                break;
            }
            String[] data = line.split(": ");
            headers.put(data[0], data[1]);
        }
        System.out.println(headers);
        System.out.println("消息头解析完成.....");
    }

    private void parseRequestLine() {
        System.out.println("开始解析请求头......");
        String line = readLine();
        String[] data = line.split("\\s");
        method = data[0];
        url = data[1];
        protocol = data[2];

        //继续解析url

        System.out.println(method);
        System.out.println(url);
        System.out.println(protocol);
        System.out.println("请求头解难析完毕");
    }
    private void parseURL(){
        System.out.println("HttpRequest:进一步解析url...");
        //判断url中是否有参数
            String[] data = url.split("\\?");
            this.requestURI = data[0];
            if(data.length>1){
            this.queryString = data[1];
            String[] paraArr = this.queryString.split("&");
            for(String para :paraArr ){
                String[] arr = para.split("=");
                if(arr.length>1){
                    this.parameters.put(arr[0],arr[1]);
                }else {
                    this.parameters.put(arr[0],null);
                }
            }
        }
        //拆分
    }
    private String readLine() {
        StringBuilder sb = new StringBuilder();
        //上次读取到的地方
        int pre = -1;
        //本次读到的地方
        int cur = -1;
        try {
            while ((cur = in.read()) != -1) {
                //判断上次读取的是否为CR，本次是否为LF
                if (pre == 13 && cur == 10) {
                    break;
                }
                sb.append((char) cur);
                pre = cur;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString().trim();
    }

    public String getMethod() {
        return method;
    }

    public String getUrl() {
        return url;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getHeader(String name) {
        return headers.get(name);
    }

    public String getParamerts(String key) {
        return this.parameters.get(key);
    }
}
