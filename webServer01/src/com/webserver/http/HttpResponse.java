package com.webserver.http;


import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Map;
import java.util.Set;

public class HttpResponse {
    private File entity;
    private Socket socket;
    private OutputStream out;
    private  int statusCode = 200;
    private String statusResponse = "OK" ;

    //响应头相关信息定义
    private Map<String,String> headers = new HashMap<String,String>();


    public HttpResponse(Socket socket) {
        try {
            this.socket = socket;
            this.out = socket.getOutputStream();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 将当前响应对象内容按照标准的HTTP响应格式发送给
     * 客户端。
     */
    public void flush() {
        /*
         * 1:发送状态行
         * 2:发送响应头
         * 3:发送响应正文
         */
        sendStatusLine();
        sendHeadler();
        sendContent();
    }

    /**
     * 发送状态行
     */
    private void sendStatusLine() {
        System.out.println("发送状态行...");
        try {
            String line = "HTTP/1.1"+ statusCode+" "+ statusResponse;
            out.write(line.getBytes("ISO8859-1"));
            out.write(13);
            out.write(10);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 发送响应头
     */
    private void sendHeadler() {
        System.out.println("发送响应头....");
        try {
          Set<Entry<String,String>> entrySet = headers.entrySet();
                for(Entry<String,String> headers : entrySet) {
                    String key = headers.getKey();
                    String value = headers.getValue();
                    String line = key+": "+value;
                    out.write(line.getBytes("ISO8859-1"));
                    out.write(13);
                    out.write(10);
                }
            out.write(13);
            out.write(10);

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("响应头发送完毕....");
    }

    /**
     * 发送正文
     */
    private void sendContent() {
        System.out.println("开发送正文......");
        try {
            FileInputStream fis = new FileInputStream(entity);
            int len = -1;
            byte[] data = new byte[1024 * 10];
            while ((len = fis.read(data)) != -1) {
                out.write(data, 0, len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("响应正文以发送完毕...");
    }

    public File getEntity() {
        return entity;
    }

    public void setEntity(File entity) {
        /*
         * 添加Content-Type
         * 根据给定的文件的名字的后缀，从HttpContext
         * 中获取对应的Content-Type值
         */
        String fileName = entity.getName();
        int index = fileName.lastIndexOf(".")+1;
        String ext = fileName.substring(index);
        String contentType=HttpContext.getMineType(ext);

        this.headers.put("Content-Lenthg",entity.length()+"");
        this.entity = entity;
    }

    public String getStatusResponse() {
        return statusResponse;
    }

    public void setStatusResponse(String statusResponse) {
        this.statusResponse = statusResponse;

    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
        this.statusResponse =HttpContext.getStatusReason(statusCode);
    }

    /**
     * 添加指定的响应头
     * @param name
     * @param value
     */
    public void putHeader(String name,String value) {
        this.headers.put(name, value);
    }

    public String getHeader(String name) {
        return this.headers.get(name);
    }


}
